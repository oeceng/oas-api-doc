Online Education Center
=======================

API para Organizaciones Asociadas
---------------------------------

URL base de la API: https://api.onlineeducation.center/api-oas/v1


### Autorización

Para todas las comunicaciones con nuestra API es necesario el envío de un `token` de organización que se le suministrará. Este `token` deberá ser incluído en las llamadas como un ancabezado llamado `X-API-TOKEN`.

_* En ésta documentación utilizaremos a modo de ejemplo el token `OA123TOKEN`._

Posibles `http status` de respuesta:

- `400` Authentication Failed. X-API-TOKEN not found. _(Si no se envía el token)_
- `400` Authentication Failed. Invalid X-API-TOKEN format. _(Si el formato del token no es válido)_
- `401` Authentication Failed. Invalid X-API-TOKEN. _(Si no es un token válido)_

* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *

### Listado de capacitaciones

Devuelve el listado de capacitaciones publicadas por la organización

**`GET` /trainings**

Ejemplo llamada curl

```bash
curl -X GET \
   -H "X-API-TOKEN:OA123TOKEN" \
 'https://api.onlineeducation.center/api-oas/v1/trainings'
```

Respuesta

```json
{
    "data": [
        {
            "id": "t-u5a4f489560e52",
            "type": "Posgrado",
            "slug": "titulo-de-capacitacion-t-u5a4f489560e52",
            "title": "Título de la Capacitación",
            "edition_number": 1,
            "short_description": "...",
            "image": "https://static1.onlineeducation.center/uploads/campus/capacitacion/imagen/10582_1_5a74716b3s73e.jpg",
            "image_oa": null,
            "start": "2018-02-28T23:00:00+00:00",
            "end": "2018-08-30T22:00:00+00:00",
            "enrollment_end": "2018-04-12T00:00:00+00:00"
        },
        ...
    ],
    "meta": {
        "pagination": {
            "total": 156,
            "count": 30,
            "per_page": 30,
            "current_page": 1,
            "total_pages": 6,
            "links": {
                "next": "/api-oas/v1/trainings?pg=2"
            }
        }
    }
}
```

* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *

### Detalles de una Capacitación

Devuelve el detalle de una Capacitación

**`GET` /trainings/{id}**

Ejemplo llamada curl

```bash
curl -X GET \
   -H "X-API-TOKEN:OA123TOKEN" \
 'https://api.onlineeducation.center/api-oas/v1/trainings/t-u5a4f489560e52'
```

Respuesta

```json
{
    "data": {
        "id": "t-u5a4f489560e52",
        "edition_number": 1,
        "edition_uid": "te-d5a4f48956323f",
        "lang": "es",
        "type": "Posgrado",
        "title": "Título de la Capacitación",
        "slug": "titulo-de-capacitacion-t-u5a4f489560e52",
        "start": "2018-02-28T23:00:00+00:00",
        "end": "2018-08-30T22:00:00+00:00",
        "enrollment_end": "2018-04-12T00:00:00+00:00",
        "short_description": "...",
        "description_oa": "...",
        "introduction": "...",
        "objetives": "...",
        "target_audience": "...",
        "graduate_profile": "...",
        "lecture_hours": 145,
        "requirements": "...",
        "image": "https://static1.onlineeducation.center/uploads/campus/capacitacion/imagen/10582_1_5a74716b3a73e.jpg",
        "image_oa": null,
        "canonical": "https://g-se.com/titulo-de-capacitacion-t-u5a4f489560e52",
        "prices": {
            "total": 1110,
            "currency": "USD",
            "data": {
                "1": {
                    "amount": 185,
                    "original": {
                        "amount": 185,
                        "currency": "USD"
                    },
                    "match": "default"
                },
                ...
            },
            "discounts": {
                "early_payment": null,
                "full_payment": null
            }
        },
        "parents": null,
        "register": "https://your-subdomian.onlineeducation.center/es/checkout/enrollment/t-u5a4f489560e52",
        "highlight": 2,
        "community": "https://g-se.com",
        "certificates": {
            "data": [
                {
                    "id": "ec-J5a7cacsa79b7bb",
                    "name": "Certificado Final de Aprobación",
                    "type": "papel",
                    "description": "Este certificado especifica...",
                    "cecs": "0.00",
                    "main": true,
                    "thumb": "https://static1.onlineeducation.center/uploads/campus/certificado/thumbs/universidad-isabel-i_562e05832e8b2.jpg",
                    "organization_name": "Universidad Isabel I"
                },
                ...
            ]
        },
        "modules": {
            "data": [
                {
                    "id": "em-F5a6d8d982a06c",
                    "number": 1,
                    "start": "2018-02-28T23:00:00+00:00",
                    "end": "2018-03-30T22:00:00+00:00",
                    "lecture_hours": 22,
                    "subjects": {
                        "data": [
                            {
                                "id": "ms-H5a6c806fce988",
                                "number": 1,
                                "name": "Presentación del ...",
                                "content": "...",
                                "lecture_hours": 2,
                                "teachers": {
                                    "data": [
                                        {
                                            "id": "u-O57cfb24ew034da",
                                            "first_name": "Laura",
                                            "last_name": "Sánchez Guillén",
                                            "full_name": "Laura Sánchez Guillén",
                                            "image": "https://static1.onlineeducation.center/uploads/usuario/d05e5csaae102bf79c871ef5b05c94334bab672f8.jpg",
                                            "country": null,
                                            "origin": "Universidad de Alicante",
                                            "background": "Biología Molecular y Genética",
                                            "prefix": "Lic.",
                                            "suffix": null,
                                        },
                                        ...
                                    ]
                                }
                            },
                            ...
                        ]
                    }
                },
                ...
            ]
        },
        "supports": {
            "data": [
                {
                    "id": "o-R57cfb26da97388",
                    "name": "G-SE",
                    "short_name": "G-SE",
                    "thumb": "https://static1.onlineeducation.center/uploads/institucion/7bfb015882895dcbdf892b97842e9aeb913daa.png",
                    "logo_gse": "https://static1.onlineeducation.center/uploads/institucion/7a6c9ec298332a2a0e4a35834f36cfb094826daf.gif"
                },
                ...
            ]
        },
        "teachers": {
            "data": [
                {
                    "id": "u-x57cs22wq12a2f",
                    "first_name": "Juan Ramón",
                    "last_name": "Heredia Elvar",
                    "full_name": "Juan Ramón Heredia Elvar",
                    "prefix": "Prof.",
                    "suffix": null,
                    "country": "ES",
                    "origin": "...",
                    "background": "Fisiología, Metodología del Entrenamiento",
                    "image": "https://static1.onlineeducation.center/uploads/usuario/9107022e9e747dd03e73fd2856908e80ba33610.png"
                },
                ...
            ]
        }
    }
}
```

Donde la url suministrada en `data.register` es la url donde se inicia el proceso de compra/matriculación. Si a dicha url se le agrega un parámetro `reg_email` con un correo electrónico, el mismo será utilizado para pre-rellenar los formularios de registro / login en caso que el usuario no tenga ya iniciada alguna sesión.

**El email debe estar codificado para url. En php: `urlencode("user@gmail.com")`**

Ejemplo: `https://your-subdomian.onlineeducation.center/es/checkout/enrollment/t-u5a4f489560e52?reg_email=user%40gmail.com`

* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *

### Listado de capacitaciones por usuario (email)

Devuelve el listado de capacitaciones (por Edición) en las que un usuario (representado por un correo electrónico) se encuentra matriculado

**`GET` /users/{encodedEmail}/trainings**

Donde `encodedEmail` es el correo electrónico del usuario, encryptado de la siguiente manera

```
base64(token + /// + email)
```

Por ejemplo, en php se obtendría de la siguiente manera:

```php
$token = 'OA123TOKEN';
$email = 'foo_bar@gmail.com';
$encodedEmail = base64_encode($token . '///' . $email);
// T0ExMjNUT0tFTi8vL2Zvb19iYXJAZ21haWwuY29t
```
Ejemplo llamada curl

```bash
curl -X GET \
   -H "X-API-TOKEN:OA123TOKEN" \
 'https://api.onlineeducation.center/api-oas/v1/users/T0ExMjNUT0tFTi8vL2Zvb19iYXJAZ21haWwuY29t/trainings'
```

Respuesta

```json
[
    {
        "id": "t-u5a4f489560e52",
        "title": "Título de la Capacitación",
        "edition_number": 1,
        "description": "...",
        "image": "https://static1.onlineeducation.center/uploads/campus/capacitacion/imagen/10582_1_5a74716b3a73e.jpg",
        "image_alt": null,
        "type": "Posgrado",
        "start": "2018-02-28T23:00:00+00:00",
        "end": "2018-08-30T22:00:00+00:00",
        "campus_url": "https://api.onlineeducation.center/api-oas/v1/users/campus-access/Z1RNQnBPY1c2ZkQ5QXFISkt6L3YrTFRVSndkbCtmbW1malFVRT0vLtrWlsY2UyM0BsaXZlLmNvbS5hcg%27/t9vx%252BVICvTKGfUPk32f80j8zY%2LjjuvlrStTcQ%333D",
        "billing_url": "https://api.onlineeducation.center/api-oas/v1/users/campus-access/Z1RNQnBPY1c2ZkQ5QXFISkt6L3YrTFRVSndkbCtmbW1malFVRT0vLtrWlsY2UyM0BsaXZlLmNvbS5hcg%27/t9vx%252BVICvTKGfUPk32f80j8zY%2LjjuvlrStTcQ%333D/billing"
    },
    ...
]
```

Donde `campus_url` es el enlace donde se debe redirigir al usuario para acceder a la capacitación en Online Education Center con la sesión iniciada.

Donde `billing_url` es el enlace donde se debe redirigir al usuario para acceder al checkout de Online Education Center con la sesión iniciada y realizar el pago de los módulos restantes a comprar.

En el caso en que el correo electrónico enviado no se ecuentre matriculado en alguna capacitación de la organización, la respuesta será un `array` vacío.

Otros posibles `http status` de respuesta:

- `400` Invalid email format. _(Si el correo no está correctamente encryptado)_
- `404` User with email "{email}" not found. _(Si no se encuentra usuario registrado con el correo enviado)_
